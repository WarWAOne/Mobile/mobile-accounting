import 'package:flutter/material.dart';

/// All colors used in the app

const PRIMATY_COLOR = Color(0xFFf5f5f5);
const ACCENT_COLOR = Color(0xFF304ffe);
const LIGHT_TEXT_COLOR = Colors.white;
const DARK_TEXT_COLOR = Colors.black87;