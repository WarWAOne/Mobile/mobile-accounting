import 'package:accounting/res/colors.dart' as ResColors;
import 'package:accounting/res/strings.dart' as Strings;
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:accounting/components/Revenue/RevenueListPage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    return new MaterialApp(
      title: Strings.APP_NAME,
      theme: new ThemeData(
          fontFamily: "Monospace",
          scaffoldBackgroundColor: ResColors.PRIMATY_COLOR,
          accentColor: ResColors.ACCENT_COLOR,
          primaryColor: ResColors.ACCENT_COLOR),
      home: new RevenueListPage(title: Strings.TITLE_REVENUE_LIST_PAGE),
    );
  }
}