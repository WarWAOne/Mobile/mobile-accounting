import 'package:flutter/material.dart';

/// Action object class : an action is an icon button in the appbar
/// that allow to do something

class Action {
  const Action({this.title, this.icon});

  final String title;
  final IconData icon;
}
