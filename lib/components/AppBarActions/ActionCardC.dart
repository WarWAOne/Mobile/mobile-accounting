import 'package:flutter/material.dart';
import 'package:accounting/components/AppBarActions/Action.dart';

/// Component Action card is the icon button that is put in the appbar

class ActionCardC extends StatelessWidget {
  const ActionCardC({Key key, this.choice}) : super(key: key);

  final Action choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}
