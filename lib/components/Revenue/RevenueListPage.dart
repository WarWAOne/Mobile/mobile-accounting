import 'package:accounting/components/Revenue/Revenue.dart';
import 'package:accounting/components/Revenue/RevenueAlertDialogC.dart';
import 'package:accounting/components/Revenue/RevenueItemC.dart';
import 'package:accounting/components/Revenue/RevenueTable.dart';
import 'package:accounting/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

/// Page that display a list of revenues with all possible interactions

const DB_FILE = "Accounting.db";

class RevenueListPage extends StatefulWidget {
  RevenueListPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _RevenueListPageState createState() => new _RevenueListPageState();
}

class _RevenueListPageState extends State<RevenueListPage> {
  List<Revenue> list = new List(); // List of revenues
  RevenueTable table = new RevenueTable(); // Table of revenues
  double total = 0.0;

  /// Add a revenue to the database and the list asynchronously
  /// @param revenue
  Future<void> addRevenue(Revenue revenue) async {
    await table
        .open(table.dbPath)
        .then((data) => table.insert(revenue).then((revenue) => setState(() {
              list.add(revenue);
            })));
    table.close();
  }

  /// Create and/or open the database asynchronously
  Future<void> createOpenDatabase() async {
    final dir = await getApplicationDocumentsDirectory();
    await table.open(dir.path + '/' + DB_FILE);
  }

  /// Retreive revenue list asynchronously
  Future<void> initList() async {
    await createOpenDatabase();
    final revenues = await table.getAll();
    list = revenues;
    total = await table.getTotalAmount();
    table.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title:
              new Text(widget.title, style: TextStyle(color: DARK_TEXT_COLOR)),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          brightness: Brightness.light,
        ),
        body: FutureBuilder<void>(
          future: initList(),
          builder: (context, snap) {
            if (snap.connectionState == ConnectionState.done) {
              return CustomScrollView(slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.transparent,
                  expandedHeight: 70.0,
                  floating: true,
                  elevation: 0.0,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(total.toStringAsFixed(2) + "€",
                        style:
                            TextStyle(color: DARK_TEXT_COLOR, fontSize: 17.0)),

                  ),
                ),
                SliverFixedExtentList(
                    delegate: SliverChildBuilderDelegate(
                      (context, i) {
                        return new RevenueItem(list[i], table);
                      }, childCount: list.length
                    ),
                    itemExtent: 50.0),
              ]);
            } else
              return Center(child: Text('Loading...'));
          },
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: ACCENT_COLOR,
            onPressed: () {
              showDialog(
                  context: context,
                  child: RevenueAlertDialogC(
                      list: list,
                      addRevenue: addRevenue)); //_buildDialog(context));
            },
            child: Icon(Icons.add)));
  }
}
