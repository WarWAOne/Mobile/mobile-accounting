import 'package:flutter/material.dart';
import 'package:accounting/components/Revenue/RevenueDetailsPage.dart';
import 'package:accounting/components/Revenue/RevenueTable.dart';
import 'package:accounting/components/Revenue/Revenue.dart';
import 'package:accounting/res/colors.dart';

/// Component RevenueItem to display in a list

class RevenueItem extends StatelessWidget {
  final Revenue revenue;
  final RevenueTable table;

  RevenueItem(this.revenue, this.table);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        child: new Padding(
          padding: EdgeInsets.all(16.0),
          child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Text(revenue.label),
                new Text(revenue.amount.toStringAsFixed(2) + " €",
                    style: TextStyle(fontWeight: FontWeight.bold, color: ACCENT_COLOR)),
              ]),
        ),
        onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      RevenueDetailsPage(revenue: revenue, table: table)),
            ));
  }
}
