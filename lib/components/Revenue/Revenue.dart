// Revenue tableColumns
final String tableRevenue = "revenue";
final String colId = "_id";
final String colAmount = "amount";
final String colLabel = "label";

/// Revenue class object
class Revenue {

// Same as columns
  int id;
  String label;
  double amount;

  Revenue();

  /// Function that allow the object to be iterable   
  ///  like a Map with a string as key. That allow user
  ///  to get each column value of the Object (id, label...)
  /// @return Map<columnName, value> of the revenue in question (all columns and his values)
  Map<String, dynamic> toMap() {
    /* Create the map of the object with column
        name as key and value as value */
    var map = <String, dynamic>{
      colLabel: label,
      colAmount: amount,
    };
    if (id != null) {
      map[colId] = id;
    }
    return map;
  }

// Convert Map to Revenue
  Revenue.fromMap(Map<String, dynamic> map) {
    id = map[colId];
    label = map[colLabel];
    amount = map[colAmount];
  }

/// Convert list of maps to list of revenues
/// @param List of revenues as Map
/// @return List of Revenue
  static List<Revenue> asList(List<Map> maps) {
    List<Revenue> list = new List();
    maps.forEach((revenue) => list.add(Revenue.fromMap(revenue)));
    return list;
  }
}
