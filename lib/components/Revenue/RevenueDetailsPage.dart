import 'package:accounting/components/AppBarActions/Action.dart';
import 'package:accounting/components/Revenue/Revenue.dart';
import 'package:accounting/components/Revenue/RevenueEditPage.dart';
import 'package:accounting/components/Revenue/RevenueTable.dart';
import 'package:accounting/res/colors.dart';
import 'package:flutter/material.dart';
import 'dart:async';

/// Page (states) to display details of a revenue
/// @state revenue to display
/// @state revenue table

// List of the actions displayed in the appbar
const List<Action> actions = const <Action>[
  const Action(title: 'delete', icon: Icons.delete),
];

class RevenueDetailsPage extends StatefulWidget {
  RevenueDetailsPage({Key key, this.revenue, this.table}) : super(key: key);

  final Revenue revenue;
  final RevenueTable table;

  @override
  _RevenueDetailsPageState createState() => new _RevenueDetailsPageState();
}

class _RevenueDetailsPageState extends State<RevenueDetailsPage> {
  Action _selectedAction = actions[0];

  /// Remove a revenue from the DB
  /// @param revenue to remove
  Future<void> removeRevenue(Revenue revenue) async {
    await widget.table.open(widget.table.dbPath);
    await widget.table.delete(widget.revenue.id);
  }

  /// Select one of the action defined
  void _select(Action action) {
    setState(() {
      _selectedAction = action;
    });

// Foreach action
    switch (action.title) {
      case "delete": // Remove a revenue
        removeRevenue(widget.revenue);
        widget.table.close();
        Navigator.pop(context);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text('Details', style: TextStyle(color: DARK_TEXT_COLOR)),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: DARK_TEXT_COLOR),
        actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(actions[0].icon),
            color: DARK_TEXT_COLOR,
            onPressed: () {
              _select(actions[0]);
            },
          ),
        ],
      ),
      body: new Column(
        children: <Widget>[
          const SizedBox(height: 50.0),
          new Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            new Text(widget.revenue.label,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0)),
          ]),
          const SizedBox(height: 50.0),
          new Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            new Text(widget.revenue.amount.toString() + " €",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: ACCENT_COLOR)),
          ]),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          //_buildDialog(context));
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RevenueEditPage(
                    revenue: widget.revenue, table: widget.table)),
          );
        },
        child: new Icon(Icons.edit),
      ),
    );
  }
}
