import 'package:accounting/components/Revenue/Revenue.dart';
import 'package:accounting/components/Revenue/RevenueTable.dart';
import 'package:flutter/material.dart';
import 'package:accounting/res/colors.dart';
import 'dart:async';

/// Page to edit a revenue

class RevenueEditPage extends StatefulWidget {
  RevenueEditPage({Key key, this.revenue, this.table}) : super(key: key);

  final Revenue revenue;
  final RevenueTable table;

  @override
  _RevenueEditPageState createState() => new _RevenueEditPageState();
}

class _RevenueEditPageState extends State<RevenueEditPage> {
  // Edit text controllers
  final labelController = TextEditingController();
  final amountController = TextEditingController();

  @override
  void dispose() {
    amountController.dispose();
    labelController.dispose();
    super.dispose();
  }

/// Update a revenu
/// @param revenue to edit
  Future<void> updateRevenue(Revenue revenue) async {
    await widget.table.open(widget.table.dbPath);
    await widget.table.update(revenue);
  }

  @override
  Widget build(BuildContext context) {
    Revenue revenue = widget.revenue;
    labelController.text = revenue.label;
    amountController.text = revenue.amount.toString();

    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text('Edit Revenue', style: TextStyle(color: DARK_TEXT_COLOR)),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: DARK_TEXT_COLOR),
      ),
      body: Padding(
        padding: EdgeInsets.all(40.0),
        child: Column(
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              Expanded(
                child: TextField(
                  decoration: InputDecoration(labelText: 'Label'),
                  autofocus: true,
                  controller: labelController,
                ),
              ),
            ]),
            const SizedBox(height: 50.0),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              Expanded(
                  child: TextField(
                controller: amountController,
                decoration: InputDecoration(labelText: 'Amount'),
              )),
            ]),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          revenue.label = labelController.text;
          revenue.amount = double.parse(amountController.text);
          updateRevenue(revenue);
          Navigator.pop(context);
          widget.table.close();
        },
        child: new Icon(Icons.check),
      ),
    );
  }
}
