import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:accounting/components/Revenue/Revenue.dart';

// Table name and its columns
final String tableRevenue = "revenue";
final String colId = "_id";
final String colAmount = "amount";
final String colLabel = "label";

/// Revenue Table
class RevenueTable {
  Database db;
  String dbPath;

  /// Open Database and create it if not exists
  /// @param path to the .db file
  Future open(String path) async {
    dbPath = path;
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableRevenue ( 
  $colId integer primary key autoincrement, 
  $colLabel text not null,
  $colAmount real not null)
''');
    });
  }

  /// Insert element in the table
  /// @param revenue to insert
  /// @return Future inserted revenue
  Future<Revenue> insert(Revenue revenue) async {
    revenue.id = await db.insert(tableRevenue, revenue.toMap());
    return revenue;
  }

  /// Get one revenue
  /// @param revenue id
  /// @return the revenur with this id
  Future<Revenue> getRevenue(int id) async {
    List<Map> maps = await db.query(tableRevenue,
        columns: [colId, colLabel, colAmount],
        where: "$colId = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return new Revenue.fromMap(maps.first);
    }
    return null;
  }

  /// Get all revenues
  /// @return List of all revenues
  Future<List<Revenue>> getAll() async {
    List<Map> maps =
        await db.query(tableRevenue, columns: [colId, colLabel, colAmount]);

    if (maps.length > 0) {
      return Revenue.asList(maps);
    }

    return null;
  }

  /// Get total of the amounts
  /// @return int total amount
  Future<double> getTotalAmount() async {
    List<Map> maps =
        await db.query(tableRevenue, columns: ["SUM(" + colAmount + ") as total"]);
    
    if (maps.length > 0) {
      return maps.first["total"];
    }
    //int sum = await db.rawQuery("select SUM("+colAmount+") from "+tableRevenue);
    //double sum =  await db.execute("select SUM("+colAmount+") from "+tableRevenue);
    return 0.0;
  }

  /// Remove a revenue from the table
  /// @param revenue id
  /// @return int
  Future<int> delete(int id) async {
    return await db.delete(tableRevenue, where: "$colId = ?", whereArgs: [id]);
  }

  /// Update a revenue
  /// @param the revenue with modifications
  /// @return int
  Future<int> update(Revenue revenue) async {
    return await db.update(tableRevenue, revenue.toMap(),
        where: "$colId = ?", whereArgs: [revenue.id]);
  }

  /// Close the connection to the db
  Future close() async => db.close();
}
