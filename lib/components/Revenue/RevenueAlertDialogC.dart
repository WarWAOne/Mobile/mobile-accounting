import 'package:flutter/material.dart';
import 'package:accounting/components/Revenue/Revenue.dart';

/// Component Alert Dialog to add a new revenues to the DB

class RevenueAlertDialogC extends StatefulWidget {
  RevenueAlertDialogC({Key key, this.list, this.addRevenue}) : super(key: key);

  final List<Revenue> list;
  final Function addRevenue;

  @override
  _RevenueAlertDialogCState createState() => new _RevenueAlertDialogCState();
}

class _RevenueAlertDialogCState extends State<RevenueAlertDialogC> {
  // Edit text controllers
  final amountController = TextEditingController();
  final labelController = TextEditingController();

// Override function dispose for matching with edit text controllers
  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    amountController.dispose();
    labelController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      title: new Text('Add new revenue'),
      content: new SingleChildScrollView(
        child: new ListBody(
          children: <Widget>[
            new TextField(
              controller: labelController,
              decoration: InputDecoration(labelText: 'Label'),
            ),
            new TextField(
              autofocus: true,
              keyboardType: TextInputType.number,
              controller: amountController,
              decoration: InputDecoration(labelText: 'Amount'),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          child: new Text('CANCEL'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        new FlatButton(
          child: new Text('SAVE'),
          onPressed: () {
            Revenue revenue = new Revenue();
            revenue.label = labelController.text;
            revenue.amount = double.parse(amountController.text);
            widget.addRevenue(revenue);
            amountController.clear();
            labelController.clear();
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
